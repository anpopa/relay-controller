# CMake general setup
cmake_minimum_required(VERSION 3.7.2)
project(Relay-Controller)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# BaseSoftwareInfrastructure
set(BSWINFRA_DIR ${CMAKE_CURRENT_SOURCE_DIR}/bswinfra)
set(CMAKE_MODULE_PATH "${BSWINFRA_DIR}/cmake" "${INTERFACES_DIR}/cmake")

message (STATUS "Module paths: ${CMAKE_MODULE_PATH}")

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "RelWithDebInfo" CACHE STRING
        "Choose the type of build: Debug, Release, RelWithDebInfo, MinSizeRel." FORCE)
endif()

# Add support for coverage analysis
if(CMAKE_BUILD_TYPE STREQUAL Coverage)
    set(COVERAGE_EXCLUDES
        "*/bswinfra/*"
        "*/build/*"
        "*/tests/*"
        )
    set(COVERAGE_BASE_DIR ${CMAKE_SOURCE_DIR}/source)
    set(COVERAGE_BRANCH_COVERAGE ON)
    set(COVERAGE_THRESHOLD_LINE 90)
    set(COVERAGE_THRESHOLD_FUNCTION 90)

    include(CoverageTarget)
endif()

# Build time configuration setup
if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
    execute_process(
        COMMAND git --git-dir "${CMAKE_CURRENT_SOURCE_DIR}/.git" rev-parse --short HEAD
        OUTPUT_VARIABLE GIT_SHA1
        OUTPUT_STRIP_TRAILING_WHITESPACE
        )
else(EXISTS "${CMAKE_SOURCE_DIR}/.git")
    set(GIT_SHA1 "")
endif(EXISTS "${CMAKE_SOURCE_DIR}/.git")

configure_file(
    ${CMAKE_SOURCE_DIR}/source/Defaults.h.in
    ${CMAKE_BINARY_DIR}/source/Defaults.h)

configure_file(
    ${CMAKE_SOURCE_DIR}/config/relay-controller.conf.in
    ${CMAKE_BINARY_DIR}/config/relay-controller.conf)

if (${CMAKE_SYSTEM_NAME} STREQUAL Linux)
    set(CONFIG_FILE "/etc/relay-controller.conf" CACHE PATH "Default config file path")
    install(FILES ${CMAKE_BINARY_DIR}/config/relay-controller.conf DESTINATION /etc)
else()
    set(CONFIG_FILE "/usr/local/etc/relay-controller.conf" CACHE PATH "Default config file path")
    install(FILES ${CMAKE_BINARY_DIR}/config/relay-controller.conf DESTINATION /usr/local/etc)
endif()

option(WITH_TESTS "Build test suite" N)
option(WITH_SYSLOG "Build Logger with syslog" Y)
option(WITH_TIDY "Build with clang-tidy" N)

if(WITH_TIDY)
    set(CMAKE_CXX_CLANG_TIDY "clang-tidy;-checks=*")
endif()

if(WITH_SYSLOG)
    add_compile_options("-DWITH_SYSLOG")
endif()

include(BSWInfra)

# Dependencies
find_package          (PkgConfig REQUIRED)

pkg_check_modules           (LIBMOSQUITTO libmosquitto>=1.6.0 REQUIRED)
include_directories         (${LIBMOSQUITTO_INCLUDE_DIRS})
set                         (LIBS ${LIBS} ${LIBMOSQUITTO_LIBRARIES})

if (${CMAKE_SYSTEM_NAME} STREQUAL Linux)
    pkg_check_modules       (LIBGPIOD libgpiod REQUIRED)
    include_directories     (${LIBGPIOD_INCLUDE_DIRS})
    set                     (LIBS ${LIBS} ${LIBGPIOD_LIBRARIES})
else()
    link_directories(/usr/local/lib)
    set (LIBS ${LIBS} gpio)
endif()

# Build flags
add_compile_options (
    -Wall
    -Wextra
    -Wno-unused-function
    -Wformat
    -Wno-variadic-macros
    -Wno-strict-aliasing
    -fstack-protector-strong
    -fwrapv
    -Wmissing-include-dirs
    -Wunused-parameter
    -Wuninitialized
    -Walloca
    -Wfloat-equal
    -Wshadow
    -Wcast-qual
    -Wconversion
    -Wsign-conversion
    -Werror
    -Wformat-security
    -Wcast-align
    -Wredundant-decls
    )

# Build
add_subdirectory(source)

if(WITH_TESTS)
    enable_testing()
    add_subdirectory(tests)
endif()

# Status reporting
message (STATUS "CONFIG_FILE: "          ${CONFIG_FILE})
message (STATUS "SYSTEM_TYPE: "          ${CMAKE_SYSTEM_NAME})
message (STATUS "CMAKE_BUILD_TYPE: "     ${CMAKE_BUILD_TYPE})
message (STATUS "WITH_TESTS: "           ${WITH_TESTS})
message (STATUS "WITH_SYSLOG: "          ${WITH_SYSLOG})
message (STATUS "WITH_TIDY: "            ${WITH_TIDY})
