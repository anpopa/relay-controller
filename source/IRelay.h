/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2020 Alin Popa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 */

#pragma once

#include <string>

namespace rctl
{

class IRelay
{
public:
    explicit IRelay(const std::string &name,
                    const std::string &desc,
                    const std::string &topic,
                    const std::string &dev,
                    bool activeLow,
                    int pin)
    {
        m_name = name;
        m_desc = desc;
        m_topic = topic;
        m_dev = dev;
        m_activeLow = activeLow;
        m_pin = pin;
    }

    auto getName() -> const std::string & { return m_name; }

    auto getDesc() -> const std::string & { return m_name; }

    auto getTopic() -> const std::string & { return m_topic; }

    auto getDevice() -> const std::string & { return m_dev; }

    auto getPin() -> int { return m_pin; }

    auto isActiveLow() -> bool { return m_activeLow; }

    virtual auto setState(bool state) -> int = 0;

protected:
    std::string m_name;
    std::string m_desc;
    std::string m_topic;
    std::string m_dev;
    bool m_activeLow; // true - Low, false = High
    int m_pin;
};

} // namespace rctl