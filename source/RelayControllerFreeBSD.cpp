/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2020 Alin Popa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 */

#include "RelayControllerFreeBSD.h"
#include "RelayFreeBSD.h"

#include "Application.h"

namespace rctl
{

using std::string;

RelayControllerFreeBSD::RelayControllerFreeBSD(const std::string &name, const std::string &device)
: IRelayController(name, device)
{
    m_handle = gpio_open_device(device.c_str());
}

void RelayControllerFreeBSD::addRelay(const std::string &name,
                                      const std::string &desc,
                                      const std::string &topic,
                                      bool activeLow,
                                      int pin)
{
    std::shared_ptr<RelayFreeBSD> relay
        = std::make_shared<RelayFreeBSD>(name, desc, topic, m_device, activeLow, pin, &m_handle);
    m_relays.append(relay);
    m_relays.commit();
}

void RelayControllerFreeBSD::loadEntries(void)
{
    std::shared_ptr<bswi::kf::KeyFile> kf = RCtlApp()->getOptions()->getConfigFile();
    auto sections = kf->getSections("Relay");

    for (auto &section : sections) {
        bool activeLow = false;

        if (strncmp(section.getValue("ActiveLow").value_or("false").c_str(), "true", strlen("true")) == 0) {
            activeLow = true;
        }
        
        addRelay(section.getValue("Name").value_or("Unknown"),
                 section.getValue("Description").value_or("Unknown"),
                 section.getValue("Topic").value_or("Unknown"),
                 activeLow,
                 std::stoi(section.getValue("GPIOPin").value_or("0")));
    }
}

} // namespace rctl
