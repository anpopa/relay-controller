/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2020 Alin Popa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 */
#include <chrono>
#include <ctime>
#include <iostream>
#include <ostream>
#include <string>
#include <unistd.h>

#include "Application.h"
#include "Defaults.h"
#include "Dispatcher.h"

using std::shared_ptr;
using std::string;

namespace rctl
{

static auto doConnect(const shared_ptr<Dispatcher> &mgr, const Dispatcher::Request &rq) -> bool;
static auto doDisconnect(const shared_ptr<Dispatcher> &mgr, const Dispatcher::Request &rq) -> bool;
static auto doQuit(const shared_ptr<Dispatcher> &mgr, const Dispatcher::Request &rq) -> bool;

void Dispatcher::enableEvents()
{
    RCtlApp()->addEventSource(m_queue);
}

auto Dispatcher::pushRequest(Request &request) -> bool
{
    return m_queue->push(request);
}

auto Dispatcher::requestHandler(const Request &request) -> bool
{
    switch (request.action) {
    case Dispatcher::Action::Connect:
        return doConnect(getShared(), request);
    case Dispatcher::Action::Disconnect:
        return doDisconnect(getShared(), request);
    case Dispatcher::Action::Quit:
        return doQuit(getShared(), request);
    default:
        break;
    }

    logError() << "Unknown action request";
    return false;
}

static auto doConnect([[maybe_unused]] const shared_ptr<Dispatcher> &mgr,
                      [[maybe_unused]] const Dispatcher::Request &) -> bool
{
    RCtlApp()->getConnection()->connect();
    return true;
}

static auto doDisconnect([[maybe_unused]] const shared_ptr<Dispatcher> &mgr,
                         [[maybe_unused]] const Dispatcher::Request &) -> bool
{
    RCtlApp()->getConnection()->disconnect();
    return true;
}

static auto doQuit([[maybe_unused]] const shared_ptr<Dispatcher> &,
                   [[maybe_unused]] const Dispatcher::Request &) -> bool
{
    std::cout << std::flush;
    exit(EXIT_SUCCESS);
}

} // namespace rctl
