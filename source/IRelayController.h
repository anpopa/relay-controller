/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2020 Alin Popa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 */

#pragma once

#include "IRelay.h"
#include "SafeList.h"
#include <string>

namespace rctl
{

class IRelayController
{
public:
    explicit IRelayController(const std::string &name, const std::string &device)
    {
        m_name = name;
        m_device = device;
    }

    auto setRelayStateByName(const std::string &name, bool state) -> int
    {
        int ret = -1;

        m_relays.foreach ([name, state, &ret](const std::shared_ptr<IRelay> &relay) {
            if (relay->getName() == name)
                ret = relay->setState(state);
        });

        return ret;
    }

    auto setRelayStateByTopic(const std::string &topic, bool state) -> int
    {
        int ret = -1;

        m_relays.foreach ([topic, state, &ret](const std::shared_ptr<IRelay> &relay) {
            if (relay->getTopic() == topic)
                ret = relay->setState(state);
        });

        return ret;
    }

    void foreachRelay(const std::function<void(const std::shared_ptr<IRelay> &)> &cb)
    {
        m_relays.foreach ([cb](const std::shared_ptr<IRelay> &relay) { cb(relay); });
    }

    virtual void loadEntries(void) = 0;

    virtual bool isValid(void) = 0;

    virtual void addRelay(const std::string &name,
                          const std::string &desc,
                          const std::string &topic,
                          bool activeLow,
                          int pin)
        = 0;

protected:
    std::string m_name;
    std::string m_device;
    bswi::util::SafeList<std::shared_ptr<IRelay>> m_relays {"RelayList"};
};

} // namespace rctl
