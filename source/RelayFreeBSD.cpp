/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2020 Alin Popa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 */

#include "RelayFreeBSD.h"

namespace rctl
{

using std::string;

RelayFreeBSD::RelayFreeBSD(const std::string &name,
                           const std::string &desc,
                           const std::string &topic,
                           const std::string &dev,
                           bool activeLow,
                           int pin,
                           gpio_handle_t *handle)
: IRelay(name, desc, topic, dev, activeLow, pin)
{
    m_handle = handle;
    gpio_pin_output(*m_handle, static_cast<gpio_pin_t>(m_pin));
}

auto RelayFreeBSD::setState(bool state) -> int
{
    if (m_handle == nullptr)
        return -1;

    if (!isActiveLow()) {
        if (state) {
            return gpio_pin_high(*m_handle, static_cast<gpio_pin_t>(m_pin));
        }
        return gpio_pin_low(*m_handle, static_cast<gpio_pin_t>(m_pin));
    } else {
        if (state) {
            return gpio_pin_low(*m_handle, static_cast<gpio_pin_t>(m_pin));
        }
    }

    return gpio_pin_high(*m_handle, static_cast<gpio_pin_t>(m_pin));
}

} // namespace rctl
