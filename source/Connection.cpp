/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2020 Alin Popa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 */

#include "Connection.h"
#include "Application.h"
#include "Logger.h"
#include "Options.h"

#include <chrono>
#include <string.h>
#include <unistd.h>

using namespace bswi::log;

namespace rctl
{

static void mqtt_connect_callback(struct mosquitto *mosq, [[maybe_unused]] void *obj, int result)
{
    logDebug() << "MQTT connect callback, result " << result;
    RCtlApp()->getController()->foreachRelay([mosq](const std::shared_ptr<IRelay> &relay) {
        logDebug() << "MQTT subscribe to " << relay->getName() << " with topic "
                   << relay->getTopic() << " and pin " << relay->getPin();
        mosquitto_subscribe(mosq, NULL, relay->getTopic().c_str(), 0);
    });
}

static void mqtt_message_callback([[maybe_unused]] struct mosquitto *mosq,
                                  [[maybe_unused]] void *obj,
                                  const struct mosquitto_message *message)
{
    logDebug() << "MQTT message callback";
    RCtlApp()->getController()->foreachRelay([message](const std::shared_ptr<IRelay> &relay) {
        bool match = false;

        mosquitto_topic_matches_sub(relay->getTopic().c_str(), message->topic, &match);
        if (match) {
            char tmp[128] = {0};
            bool state = false;

            memcpy(tmp, message->payload, static_cast<size_t>(message->payloadlen));
            if (strncmp(tmp,
                        RCtlApp()->getOptions()->getFor(Options::Key::PayloadOn).c_str(),
                        sizeof(tmp))
                == 0)
                state = true;

            relay->setState(state);
        }
    });
}

static void mqtt_thread_worker(void *con)
{
    auto *connection = reinterpret_cast<Connection *>(con);
    int rc = 0;
    int port = std::stoi(RCtlApp()->getOptions()->getFor(Options::Key::BrokerPort));

    rc = mosquitto_connect(connection->getMosquitto(),
                           RCtlApp()->getOptions()->getFor(Options::Key::BrokerAddress).c_str(),
                           port,
                           60);

    logDebug() << "MQTT Connect Status " << rc << " for "
               << RCtlApp()->getOptions()->getFor(Options::Key::BrokerAddress) << " port " << port;

    while (!connection->isStopped()) {
        rc = mosquitto_loop(connection->getMosquitto(), -1, 1);
        if (!connection->isStopped() && rc) {
            logInfo() << "MQTT connection error";
            std::this_thread::sleep_for(std::chrono::seconds(3));
            mosquitto_reconnect(connection->getMosquitto());
        }
    }

    logDebug() << "MQTT thread exit";
}

Connection::Connection(const std::string &id)
{
    m_mosq = mosquitto_new(id.c_str(), true, 0);
    if (m_mosq != NULL) {
        mosquitto_user_data_set(m_mosq, this);
        mosquitto_connect_callback_set(m_mosq, mqtt_connect_callback);
        mosquitto_message_callback_set(m_mosq, mqtt_message_callback);
    }
}

auto Connection::connect(void) -> int
{
    m_stop = false;

    if (m_thread == nullptr) {
        m_thread = std::make_unique<std::thread>(mqtt_thread_worker, this);
    } else {
        logWarn() << "Thread already running";
        return -1;
    }

    return 0;
}

auto Connection::disconnect(void) -> int
{
    m_stop = true;

    mosquitto_loop_stop(m_mosq, true);
    m_thread->join();
    m_thread = nullptr;

    return 0;
}

} // namespace rctl
