/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2020 Alin Popa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 */

#include "Options.h"
#include "Defaults.h"

using namespace std;

namespace rctl
{

Options::Options(const string &configFile)
{
    m_configFile = std::make_shared<bswi::kf::KeyFile>(configFile);
    if (m_configFile->parseFile() != 0) {
        logWarn() << "Fail to parse config file: " << configFile;
        m_configFile.reset();
    }
}

auto Options::getFor(Key key) -> string
{
    switch (key) {
    case Key::Device:
        if (hasConfigFile()) {
            const optional<string> prop
                = m_configFile->getPropertyValue("General", -1, "GPIODevice");
            return prop.value_or(rctlDefaults.getFor(Defaults::Default::Device));
        }
        return rctlDefaults.getFor(Defaults::Default::Device);
    case Key::PayloadOn:
        if (hasConfigFile()) {
            const optional<string> prop
                = m_configFile->getPropertyValue("General", -1, "PayloadOn");
            return prop.value_or(rctlDefaults.getFor(Defaults::Default::PayloadOn));
        }
        return rctlDefaults.getFor(Defaults::Default::PayloadOn);
    case Key::PayloadOff:
        if (hasConfigFile()) {
            const optional<string> prop
                = m_configFile->getPropertyValue("General", -1, "PayloadOff");
            return prop.value_or(rctlDefaults.getFor(Defaults::Default::PayloadOff));
        }
        return rctlDefaults.getFor(Defaults::Default::PayloadOff);
    case Key::BrokerAddress:
        if (hasConfigFile()) {
            const optional<string> prop = m_configFile->getPropertyValue("Broker", -1, "Address");
            return prop.value_or(rctlDefaults.getFor(Defaults::Default::BrokerAddress));
        }
        return rctlDefaults.getFor(Defaults::Default::BrokerAddress);
    case Key::BrokerPort:
        if (hasConfigFile()) {
            const optional<string> prop = m_configFile->getPropertyValue("Broker", -1, "Port");
            return prop.value_or(rctlDefaults.getFor(Defaults::Default::BrokerPort));
        }
        return rctlDefaults.getFor(Defaults::Default::BrokerPort);
    default:
        logError() << "Unknown option key";
        break;
    }

    throw std::runtime_error("Cannot provide option for key");
}

} // namespace rctl
